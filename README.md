****Lab: MNIST Clustering using kmeans****

This nootebook contains a PYTHON implementation of image clustering, based on kmeans algorithm. K-means clustering (MacQueen 1967) is one of the most commonly used unsupervised machine learning algorithm for partitioning a given data set into a set of k groups (i.e. k clusters), where k represents the number of groups pre-specified by the analyst. It classifies objects in multiple groups (i.e., clusters), such that objects within the same cluster are as similar as possible (i.e., high intra-class similarity), whereas objects from different clusters are as dissimilar as possible (i.e., low inter-class similarity). In k-means clustering, each cluster is represented by its center (i.e, centroid) which corresponds to the mean of points assigned to the cluster.

This Lab demonstartes the use of kmeans, an algorithm of unsupervided learning. The basic idea behind k-means clustering consists of defining clusters so that the total intra-cluster variation (known as total within-cluster variation) is minimized.

****Demo:****

To run the demo, you should complete all following TODO. The Face Recognition system includes the following main steps:

1. Import the needed packages
2. Load the dataset
3. Compute PCA directly using PYTHON library
4. Apply K-means
5. Results Analysis
6. Plot results
7. Accuracy
